
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX_LEN 512
#define MAXARGS 10
#define ARGLEN 30
#define PROMPT "Version2:- "

int execute(char* arglist[]);
char** tokenize(char* cmdline);
char* read_cmd(char*, FILE*);
int main(){
   char *cmdline;
   char** arglist;
   char* prompt = PROMPT;   
   while((cmdline = read_cmd(prompt,stdin)) != NULL){
      if((arglist = tokenize(cmdline)) != NULL){
            execute(arglist);
       //  need to free arglist
         for(int j=0; j < MAXARGS+1; j++)
	         free(arglist[j]);
         free(arglist);
         free(cmdline);
      }
  }//end of while loop
   printf("\n");
   return 0;
}
int execute(char* arglist[]){
   //save in/out
   int tmpin=dup(0);
     int tmpout=dup(1);
  
    //set the initial input 
    int fdin;
    if (infile) {
      fdin = open(infile,O_READ); 
  }
   else {
    // Use default input
    fdin=dup(tmpin);
    }
 
   int ret;
  int fdout;
   for(i=0;i<numsimplecommands; i++) {
    //redirect input
    dup2(fdin, 0);
   close(fdin);
  //setup output
  if (i == numsimplecommands­1){
    // Last simple command 
   if(outfile){
fdout=open(outfile,â€¦â€¦);
      }
      else {
        // Use default output
          fdout=dup(tmpout);
        }
      }
  
       else {
         // Not last 
         //simple command
         //create pipe
         int fdpipe[2];
        pipe(fdpipe);
        fdout=fdpipe[1];
        fdin=fdpipe[0];
     }// if/else
 
      // Redirect output
      dup2(fdout,1);
      close(fdout);
  
      // Create child process
      ret=fork(); 
      if(ret==0) {
        execvp(scmd[i].args[0], scmd[i].args);
        perror(â€œexecvpâ€);
        _exit(1);
      }
     } //  for
   
     //restore in/out defaults
     dup2(tmpin,0);
     dup2(tmpout,1);
     close(tmpin);
     close(tmpout);
  
     if (!background) {
      // Wait for last command
      waitpid(ret, NULL);
    }

}
char** tokenize(char* cmdline){
//allocate memory
   char** arglist = (char**)malloc(sizeof(char*)* (MAXARGS+1));
   for(int j=0; j < MAXARGS+1; j++){
	   arglist[j] = (char*)malloc(sizeof(char)* ARGLEN);
      bzero(arglist[j],ARGLEN);
    }
   if(cmdline[0] == '\0')//if user has entered nothing and pressed enter key
      return NULL;
   int argnum = 0; //slots used
   char*cp = cmdline; // pos in string
   char*start;
   int len;
   while(*cp != '\0'){
      while(*cp == ' ' || *cp == '\t') //skip leading spaces
          cp++;
      start = cp; //start of the word
      len = 1;
      //find the end of the word
      while(*++cp != '\0' && !(*cp ==' ' || *cp == '\t'))
         len++;
      strncpy(arglist[argnum], start, len);
      arglist[argnum][len] = '\0';
      argnum++;
   }
   arglist[argnum] = NULL;
   return arglist;
}      

char* read_cmd(char* prompt, FILE* fp){
   printf("%s", prompt);
  int c; //input character
   int pos = 0; //position of character in cmdline
   char* cmdline = (char*) malloc(sizeof(char)*MAX_LEN);
   while((c = getc(fp)) != EOF){
       if(c == '\n')
	  break;
       cmdline[pos++] = c;
   }
//these two lines are added, in case user press ctrl+d to exit the shell
   if(c == EOF && pos == 0) 
      return NULL;
   cmdline[pos] = '\0';
   return cmdline;
}

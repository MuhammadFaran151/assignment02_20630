Status : Code is in full running state except the use of getcwd() function use. I am unable to give value of getcwd buff to the prompt pointer.  

Features: Implemented system call execution, use crtl+D command to quit. 

Acknowledgement : 1) http://pages.cs.wisc.edu/~remzi/Classes/537/Fall2013/Projects/p2a.html
2)https://www.geeksforgeeks.org/making-linux-shell-c/
3)https://stackoverflow.com/questions/298510/how-to-get-the-current-directory-in-a-c-program
